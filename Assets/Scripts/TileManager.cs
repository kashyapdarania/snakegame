﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileState
{
    Untouched = 0,
    Touched = 1,
}

public struct TileTouchEvent
{
    public static TileTouchEvent current;

    public Tile tile;

    public static void Trigger(Tile tile)
    {
        current.tile = tile;
        EventManager.TriggerEvent<TileTouchEvent>(current);
    }
}

public class TileManager : MonoBehaviour, EventListener<TileTouchEvent>
{
    [SerializeField]
    private List<Tile> m_Tiles;

    private void Awake()
    {
        m_Tiles = new List<Tile>();
    }

    private void OnEnable()
    {
        this.EventStartListening<TileTouchEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<TileTouchEvent>();
    }

    public void AddTile(Tile tile)
    {
        m_Tiles.Add(tile);
    }

    public Vector3 GetUntouchedTilePosition()
    {
        return m_Tiles[Random.Range(0, m_Tiles.Count)].transform.position;
    }

    public void OnEvent(TileTouchEvent eventType)
    {
        m_Tiles.Remove(eventType.tile);

        if (m_Tiles.Count == 0)
        {
            GameCompleteEvent.Trigger();
        }
    }
}
