using UnityEngine;

namespace Timeloop.Helper
{
    public class MultiResolution
    {
        public const float SPACING_ON_SIDES_OF_GRID = 0.1f;
        public static void SetupWorld(Transform transform, int rows, int cols, Vector3 gridSize)
        {
            // fit in height (keep gap so there is visible area surrounding grid)
            float l_CameraYPos = Screen.height / (1.8f * 100);
            Camera.main.orthographicSize = l_CameraYPos;

            if (Screen.orientation == ScreenOrientation.Landscape || true)
            {
                if ((cols / (float)rows) > ScaleFactor.value)
                {
                    transform.localScale *= (Screen.width / (gridSize.x * Constant.PIXEL_PER_UNIT));
                }
                else
                {
                    transform.localScale *= (Screen.height / (gridSize.y * Constant.PIXEL_PER_UNIT));
                }
            }
            else if (Screen.orientation == ScreenOrientation.Portrait)
            {
                if ((cols / (float)rows) > ScaleFactor.value)
                {
                    transform.localScale *= (Screen.height / (gridSize.y * Constant.PIXEL_PER_UNIT));
                }
                else
                {
                    transform.localScale *= (Screen.width / (gridSize.x * Constant.PIXEL_PER_UNIT));
                }
            }
        }
    }
}