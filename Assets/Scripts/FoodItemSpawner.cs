﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct RandomFloat
{
    public float min;
    public float max;

    public float Value
    {
        get
        {
            return UnityEngine.Random.Range(min, max);
        }
    }
}

public struct FoodItemPicked
{
    public static FoodItemPicked current;
    public FoodItem foodItem;

    public static void Trigger(FoodItem foodItem)
    {
        current.foodItem = foodItem;
        EventManager.TriggerEvent<FoodItemPicked>(current);
    }

}

public class FoodItemSpawner : MonoBehaviour, EventListener<GameStartEvent>,
                                              EventListener<GameCompleteEvent>,
                                              EventListener<FoodItemPicked>,
                                              EventListener<GamePauseEvent>,
                                              EventListener<GameResumeEvent>
{
    [SerializeField]
    private GameObject m_FoodItem;

    private TileManager m_TileManager;

    [SerializeField]
    private RandomFloat m_FoodGenerationFrequency;

    [SerializeField]
    private float m_FoodDisappearTime;

    private bool m_IsGameStarted = false;
    private bool m_IsGameCompleted = false;
    private bool m_IsGamePaused = false;

    private Coroutine m_FoodCoroutine;

    private void Awake()
    {
        m_TileManager = GetComponent<TileManager>();
        m_FoodItem.SetActive(false);
    }

    private void OnEnable()
    {
        this.EventStartListening<GameStartEvent>();
        this.EventStartListening<GameCompleteEvent>();
        this.EventStartListening<FoodItemPicked>();
        this.EventStartListening<GamePauseEvent>();
        this.EventStartListening<GameResumeEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<GameStartEvent>();
        this.EventStopListening<GameCompleteEvent>();
        this.EventStopListening<FoodItemPicked>();
        this.EventStopListening<GamePauseEvent>();
        this.EventStopListening<GameResumeEvent>();
    }

    private IEnumerator GenerateFood()
    {
        while (m_IsGameStarted && !m_IsGameCompleted)
        {
            yield return new WaitForSeconds(m_FoodGenerationFrequency.Value);

            m_FoodItem.transform.position = m_TileManager.GetUntouchedTilePosition();
            m_FoodItem.SetActive(true);

            yield return new WaitForSeconds(m_FoodDisappearTime);

            m_FoodItem.SetActive(false);
        }
    }

    public void OnEvent(GameStartEvent eventType)
    {
        m_IsGameStarted = true;
        m_IsGameCompleted = false;

        m_FoodCoroutine = StartCoroutine(GenerateFood());
    }

    public void OnEvent(GameCompleteEvent eventType)
    {
        m_IsGameCompleted = true;
        m_IsGameStarted = false;

        if (m_FoodCoroutine != null)
        {
            StopCoroutine(m_FoodCoroutine);
            m_FoodCoroutine = null;
        }
    }

    public void OnEvent(FoodItemPicked eventType)
    {
        if (m_FoodCoroutine != null)
        {
            StopCoroutine(m_FoodCoroutine);
            m_FoodCoroutine = null;
        }

        m_FoodCoroutine = StartCoroutine(GenerateFood());
    }

    public void OnEvent(GamePauseEvent eventType)
    {
        m_IsGamePaused = true;

        if (m_FoodCoroutine != null)
        {
            StopCoroutine(m_FoodCoroutine);
            m_FoodCoroutine = null;
        }
    }

    public void OnEvent(GameResumeEvent eventType)
    {
        m_IsGamePaused = false;

        if (m_FoodCoroutine != null)
        {
            StopCoroutine(m_FoodCoroutine);
            m_FoodCoroutine = null;
        }

        m_FoodCoroutine = StartCoroutine(GenerateFood());
    }
}
