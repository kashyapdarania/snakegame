﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, EventListener<GameStartEvent>,
                                               EventListener<GameCompleteEvent>,
                                               EventListener<GamePauseEvent>,
                                               EventListener<GameResumeEvent>
{
    private const float ROTATION_SPEED = 100f;
    private const float RAYCAST_DISTANCE = 0.1f;
    private const string HORIZONTAL_AXIS = "Horizontal";

    [SerializeField]
    private float m_Speed = 0;

    [SerializeField]
    private Animator m_Animator = default;

    [SerializeField]
    private Transform m_RaycastPoint = default;

    [SerializeField]
    private LayerMask m_WallLayer = default;

    [SerializeField]
    private TileManager m_TileManager = default;

    private float m_Horizontal = 0;

    private bool m_IsGameStarted = false;
    private bool m_IsGameCompleted = false;
    private bool m_IsGamePaused = false;

    private void Awake()
    {
        m_TileManager = FindObjectOfType<TileManager>();
    }

    private void OnEnable()
    {
        this.EventStartListening<GameStartEvent>();
        this.EventStartListening<GameCompleteEvent>();
        this.EventStartListening<GameResumeEvent>();
        this.EventStartListening<GamePauseEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<GameStartEvent>();
        this.EventStopListening<GameCompleteEvent>();
        this.EventStopListening<GameResumeEvent>();
        this.EventStopListening<GamePauseEvent>();
    }

    private void Update()
    {
        if (!m_IsGameStarted || m_IsGameCompleted || m_IsGamePaused)
        {
            return;
        }

        Movement();
        RaycastForWallBounce();
    }

    private void Movement()
    {
        m_Horizontal = SimpleInput.GetAxis(HORIZONTAL_AXIS);
        m_Animator.SetFloat(HORIZONTAL_AXIS, m_Horizontal);

        transform.Translate(0, m_Speed * Time.deltaTime, 0);

        transform.Rotate(0f, 0f, -m_Horizontal * ROTATION_SPEED * Time.deltaTime);
    }

    private void RaycastForWallBounce()
    {
        RaycastHit2D hit = Physics2D.Raycast(m_RaycastPoint.position, transform.up, RAYCAST_DISTANCE, m_WallLayer);
        if (hit.collider != null)
        {
            var wall = hit.collider.GetComponent<Wall>();
            var direction = hit.point - new Vector2(transform.position.x, transform.position.y);
            var reflect = Vector3.Reflect(direction, wall.Normal);

            transform.up = reflect.normalized;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("FoodItem"))
        {
            collision.gameObject.SetActive(false);
        }
    }

    public void OnEvent(GameStartEvent eventType)
    {
        Vector3 tilePosition = m_TileManager.GetUntouchedTilePosition();
        transform.up = (new Vector2(tilePosition.x, tilePosition.y) - new Vector2(transform.position.x, transform.position.z)).normalized;

        GetComponent<BoxCollider2D>().enabled = true;
        m_Animator.gameObject.SetActive(true);

        m_IsGameStarted = true;
        m_IsGameCompleted = false;
    }

    public void OnEvent(GameCompleteEvent eventType)
    {
        m_IsGameStarted = false;
        m_IsGameCompleted = true;
    }

    public void OnEvent(GamePauseEvent eventType)
    {
        m_IsGamePaused = true;
    }

    public void OnEvent(GameResumeEvent eventType)
    {
        m_IsGamePaused = false;
    }
}
