﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant
{
    public const float REFERENCE_RESOLUTION_HEIGHT = 1080f;
    public const float REFERENCE_RESOLUTION_WIDTH = 1920f;
    public const int PIXEL_PER_UNIT = 100;

    public static float ASPECT_RATIO = REFERENCE_RESOLUTION_HEIGHT / REFERENCE_RESOLUTION_WIDTH;
}
