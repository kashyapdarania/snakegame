﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class ScaleFactor : MonoBehaviour
{
    public static float value;
    private void Awake()
    {
        value = GetComponent<Canvas>().scaleFactor;
    }
}
