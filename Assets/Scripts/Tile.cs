﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    TileState m_TileState;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            OnPlayerTouched();
        }
    }

    private void OnPlayerTouched()
    {
        switch (m_TileState)
        {
            case TileState.Untouched:
                m_TileState = TileState.Touched;
                GetComponent<SpriteRenderer>().color = Color.green;

                TileTouchEvent.Trigger(this);
                break;
        }
    }
}