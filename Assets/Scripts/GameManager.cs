﻿using SimpleInputNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public struct GameStartEvent
{
    public static GameStartEvent current;

    public static void Trigger()
    {
        EventManager.TriggerEvent<GameStartEvent>(current);
    }
}

public struct GamePauseEvent
{
    public static GamePauseEvent current;
    public static void Trigger()
    {
        EventManager.TriggerEvent<GamePauseEvent>(current);
    }
}

public struct GameResumeEvent
{
    public static GameResumeEvent current;
    public static void Trigger()
    {
        EventManager.TriggerEvent<GameResumeEvent>(current);
    }
}

public struct GameCompleteEvent
{
    public static GameCompleteEvent current;

    public static void Trigger()
    {
        EventManager.TriggerEvent<GameCompleteEvent>(current);
    }
}

public class GameManager : MonoBehaviour, EventListener<GameStartEvent>,
                                          EventListener<GameCompleteEvent>
{
    [SerializeField]
    private SteeringWheel m_SteeringWheel;

    [SerializeField]
    private Button m_StartButton;

    [SerializeField]
    private Button m_PlayPauseButton;

    [SerializeField]
    private Sprite m_PlaySprite, m_PauseSprite;

    [SerializeField]
    private GameObject m_GameCompletePanel;

    private bool m_IsGamePaused;

    private void OnEnable()
    {
        this.EventStartListening<GameStartEvent>();
        this.EventStartListening<GameCompleteEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<GameStartEvent>();
        this.EventStopListening<GameCompleteEvent>();
    }

    private void Awake()
    {
        m_SteeringWheel.gameObject.SetActive(false);
        m_StartButton.gameObject.SetActive(true);
        m_PlayPauseButton.gameObject.SetActive(false);
    }

    public void OnStartButtonClicked()
    {
        GameStartEvent.Trigger();
    }

    public void OnRestartButtonClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnEvent(GameStartEvent eventType)
    {
        m_SteeringWheel.gameObject.SetActive(true);
        m_StartButton.gameObject.SetActive(false);
        m_PlayPauseButton.GetComponent<Image>().sprite = m_PauseSprite;
        m_PlayPauseButton.gameObject.SetActive(true);
    }

    public void OnPlayPauseButtonClicked()
    {
        m_IsGamePaused = !m_IsGamePaused;
        if (m_IsGamePaused)
        {
            m_SteeringWheel.gameObject.SetActive(false);
            GamePauseEvent.Trigger();
            m_PlayPauseButton.GetComponent<Image>().sprite = m_PlaySprite;
        }
        else
        {
            m_SteeringWheel.gameObject.SetActive(true);
            GameResumeEvent.Trigger();
            m_PlayPauseButton.GetComponent<Image>().sprite = m_PauseSprite;
        }
    }

    public void OnEvent(GameCompleteEvent eventType)
    {
        m_SteeringWheel.gameObject.SetActive(false);
        m_GameCompletePanel.gameObject.SetActive(true);
        m_PlayPauseButton.gameObject.SetActive(false);
    }
}
