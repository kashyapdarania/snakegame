﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour,
                                EventListener<FoodItemPicked>,
                                EventListener<GameStartEvent>,
                                EventListener<GameCompleteEvent>
{
    [SerializeField]
    private Text m_ScoreText;

    [SerializeField]
    private Text m_ScoreTextLevelComplete;

    [SerializeField]
    private GameObject m_ScorePanel;

    private int m_Score = 0;

    private void OnEnable()
    {
        this.EventStartListening<FoodItemPicked>();
        this.EventStartListening<GameStartEvent>();
        this.EventStartListening<GameCompleteEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<FoodItemPicked>();
        this.EventStopListening<GameStartEvent>();
        this.EventStopListening<GameCompleteEvent>();
    }

    private void Awake()
    {
        m_ScorePanel.SetActive(false);
        m_ScoreText.text = m_Score.ToString();
    }

    public void OnEvent(FoodItemPicked eventType)
    {
        m_Score++;
        m_ScoreText.text = m_Score.ToString();
        m_ScoreTextLevelComplete.text = "Score : " + m_Score;
    }

    public void OnEvent(GameStartEvent eventType)
    {
        m_ScorePanel.SetActive(true);
    }

    public void OnEvent(GameCompleteEvent eventType)
    {
        m_ScoreTextLevelComplete.text = "Score : " + m_Score;
    }
}
