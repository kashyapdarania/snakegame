using UnityEngine;
using System.Collections;
using Timeloop.Helper;
using System.Collections.Generic;
using System;
using System.IO;

namespace Snake.Gameplay
{
    public class GridCreator : MonoBehaviour
    {
        public const float WALL_WIDTH = 0.2f;

        [SerializeField]
        private int rows = 10;
        [SerializeField]
        private int cols = 10;

        private Vector2 gridSize = default;

        [SerializeField]
        private Transform m_GridParent = default;

        [SerializeField]
        private Transform m_WallsParent = default;

        [SerializeField]
        private Tile m_TilePrefab = default;

        [SerializeField]
        private Wall m_WallPrefab = default;

        private Vector2 cellSize;
        private Vector2 cellScale;

        private List<Vector3> m_CellPositions;

        private TileManager m_TileManager;

        private Vector2 gridOffset;

        private int m_TotalRows;
        private int m_TotalColumns;

        private void Awake()
        {
            m_CellPositions = new List<Vector3>();
            m_TileManager = GetComponent<TileManager>();
        }

        private void Start()
        {
            // y is rows and x is columns
            CreateCells(rows, cols);
        }

        void CreateCells(int rows, int cols)
        {
            m_TotalRows = rows;
            m_TotalColumns = cols;

            gridSize = new Vector2(cols, rows);

            InitCells(rows, cols); //Initialize all cells

            // create walls
            CreateWalls();

            // handle multiresolution
            MultiResolution.SetupWorld(transform, rows, cols, gridSize);
        }

        private void CreateWalls()
        {
            // right
            CreateWall(new Vector3(m_TotalColumns / 2f + (WALL_WIDTH / 2), 0, 0), new Vector3(WALL_WIDTH, m_TotalRows, 1), Vector3.left);
            // left
            CreateWall(new Vector3(-m_TotalColumns / 2f - (WALL_WIDTH / 2), 0, 0), new Vector3(WALL_WIDTH, m_TotalRows, 1), Vector3.right);
            // top
            CreateWall(new Vector3(0, m_TotalRows / 2f + (WALL_WIDTH / 2), 0), new Vector3(m_TotalColumns + (WALL_WIDTH * 2), WALL_WIDTH, 1), Vector3.down);
            // bottom
            CreateWall(new Vector3(0, -m_TotalRows / 2f - (WALL_WIDTH / 2), 0), new Vector3(m_TotalColumns + (WALL_WIDTH * 2), WALL_WIDTH, 1), Vector3.up);
        }

        private void CreateWall(Vector3 position, Vector3 scale, Vector3 normal)
        {
            Wall wall = Instantiate(m_WallPrefab);
            wall.transform.position = position;
            wall.transform.localScale = scale;
            wall.Normal = normal;
            wall.transform.SetParent(m_WallsParent);
        }

        void InitCells(int rows, int cols)
        {
            Tile cellObject = Instantiate(m_TilePrefab);

            //creates an empty object and adds a sprite renderer component -> set the sprite to cellSprite
            //catch the size of the sprite
            cellSize = cellObject.GetComponent<SpriteRenderer>().bounds.size;

            Vector2 newCellSize = new Vector2(gridSize.x / (float)cols, gridSize.y / (float)rows);

            cellScale.x = newCellSize.x / cellSize.x;
            cellScale.y = newCellSize.y / cellSize.y;

            cellSize = newCellSize;

            cellObject.transform.localScale = new Vector2(cellScale.x, cellScale.y);
            cellSize = new Vector2(cellScale.x, cellScale.y);

            gridOffset.x = -(gridSize.x / 2) + cellSize.x / 2;
            gridOffset.y = -(gridSize.y / 2) + cellSize.y / 2;

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    Vector3 pos = new Vector3(col * cellSize.x + gridOffset.x + transform.position.x, row * cellSize.y + gridOffset.y + transform.position.y, 0);

                    Tile tile = Instantiate(cellObject, pos, Quaternion.identity);
                    tile.transform.parent = m_GridParent;
                    m_CellPositions.Add(tile.transform.localPosition);
                    m_TileManager.AddTile(tile);
                }
            }

            Destroy(cellObject);
        }

        //so you can see the width and height of the grid on editor
        void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, gridSize);
        }
    }
}